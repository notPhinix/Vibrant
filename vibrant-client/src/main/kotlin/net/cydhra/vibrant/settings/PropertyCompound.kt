package net.cydhra.vibrant.settings

class PropertyCompound {
    val properties: MutableMap<String, Property<*>> = mutableMapOf()
}
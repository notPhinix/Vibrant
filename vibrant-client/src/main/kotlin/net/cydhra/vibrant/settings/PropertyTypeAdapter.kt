package net.cydhra.vibrant.settings

import com.google.gson.*
import java.lang.reflect.Type

class PropertyTypeAdapter : JsonDeserializer<Property<*>>, JsonSerializer<Property<*>> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Property<*> {
        return Property(context.deserialize(json.asJsonObject["value"], Class.forName(json.asJsonObject["class"].asString)))
    }

    override fun serialize(src: Property<*>, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        val jsonObject = JsonObject()

        jsonObject.add("value", context.serialize(src.value))
        jsonObject.addProperty("class", src.clazz.name)

        return jsonObject
    }
}
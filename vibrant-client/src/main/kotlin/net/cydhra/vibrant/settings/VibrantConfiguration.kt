package net.cydhra.vibrant.settings

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.File

/**
 *
 */
object VibrantConfiguration {

    const val SETTINGS_FOLDER = "vibrant"

    private val gson = GsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapter(Property::class.java, PropertyTypeAdapter())
            .create()

    private val file = File(SETTINGS_FOLDER, "settings.json")

    private val settings = mutableMapOf<String, VibrantSettingDelegate<*>>()
    private lateinit var values: MutableMap<String, PropertyCompound>

    internal var scheduledThread: Thread? = null

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> registerSettingDelegate(delegate: VibrantSettingDelegate<T>) {
        settings["${delegate.module.name}.${delegate.name}"] = delegate

        // if property exists, set delegate to its value, otherwise insert it.
        if (values.containsKey(delegate.module.name)) {
            if (values[delegate.module.name]!!.properties.containsKey(delegate.name)) {
                delegate.value = values[delegate.module.name]!!.properties[delegate.name]!!.value as T
            } else {
                values[delegate.module.name]!!.properties[delegate.name] = Property(delegate.value)
            }
        } else {
            values[delegate.module.name] = PropertyCompound()
            values[delegate.module.name]!!.properties[delegate.name] = Property(delegate.value)
        }

        delegate.observer = {
            values[delegate.module.name]!!.properties[delegate.name] = Property(it)
            scheduleSave()
        }
    }

    /**
     * Load configuration from file and deserialize it. It is assumed, that the file does not contain corrupt data
     */
    fun load() {
        this.values = gson.fromJson(file
                .apply { parentFile.mkdir() }
                .apply {
                    if (createNewFile()) {
                        writeText("{}")
                    }
                }
                .readText(),
                (object : TypeToken<MutableMap<String, PropertyCompound>>() {}).type
        )
    }

    /**
     * Schedule a save. The save is executed shortly afterwards, unless a new save was scheduled. Settings can be changed rapidly without
     * spamming IO operations this way
     */
    fun scheduleSave() {
        synchronized(VibrantConfiguration) {
            scheduledThread = SaveScheduler()
            scheduledThread!!.start()
        }
    }

    /**
     * Save the configuration to a file. This should not be called from outside, use [scheduleSave] instead.
     */
    fun save() {
        file
                .apply { parentFile.mkdir() }
                .apply {
                    if (createNewFile()) {
                        writeText("{}")
                    }
                }
                .writeText(
                        gson.toJson(this.values)
                )
    }

    fun getModuleSettingDelegates(module: String): Array<VibrantSettingDelegate<*>> {
        return settings.keys
                .filter { it.toLowerCase().startsWith("${module.toLowerCase()}.") }
                .map { settings[it]!! }
                .toTypedArray()
    }

    class SaveScheduler : Thread() {
        override fun run() {
            Thread.sleep(1000L)

            synchronized(VibrantConfiguration) {
                if (VibrantConfiguration.scheduledThread == this) {
                    VibrantConfiguration.scheduledThread = null
                    VibrantConfiguration.save()
                }
            }
        }
    }
}
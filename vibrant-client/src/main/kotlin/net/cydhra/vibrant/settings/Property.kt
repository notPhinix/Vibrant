package net.cydhra.vibrant.settings

data class Property<T : Any>(val value: T) {
    val clazz: Class<T> = value.javaClass
}
package net.cydhra.vibrant.gui

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.VibrantClient.logger
import net.cydhra.vibrant.api.gui.VibrantGuiController
import net.cydhra.vibrant.api.gui.VibrantGuiScreen
import net.cydhra.vibrant.settings.VibrantConfiguration
import org.cef.CefApp
import org.cef.CefClient
import org.cef.CefSettings
import org.cef.OS
import org.cef.browser.CefBrowserOsrLwjgl
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

/**
 * A GUI module is a singleton class handling everything in a GUI screen that is persistent over multiple screen instances.
 *
 * @param name the name of the GUI module. Used to identify the module description file
 */
abstract class AbstractVibrantGuiManager(val name: String) {
    companion object {
        private val gson: Gson = GsonBuilder()
                .setPrettyPrinting()
                .create()

        val symFiles = arrayOf("icudtl.dat", "natives_blob.bin", "snapshot_blob.bin")

        val settings = CefSettings().also {
            it.windowless_rendering_enabled = true
            it.cache_path = File(VibrantConfiguration.SETTINGS_FOLDER, "cache").absolutePath
            it.log_file = File(VibrantConfiguration.SETTINGS_FOLDER, "cef.log").absolutePath
            // linux requires special treatment
            if (OS.isLinux()) {
                with(getJcefLibPath()) {
                    // the locales and resources paths are not correctly constructed, if the library path is not absolute.
                    // This code works around that.
                    logger.debug("cef path: $this")
                    it.locales_dir_path = this + "/locales"
                    it.resources_dir_path = this

                    // three files need symlinks in the JRE_HOME/bin folder
                    val javaBin = File(System.getProperty("java.home") + File.separator + "bin")

                    // create symbolic links to the binary data
                    File(this).listFiles()
                            .filter { file -> symFiles.any { sFile -> file.name.endsWith(sFile) } }
                            .forEach { file ->
                                val linkLocation = File(javaBin.absolutePath + File.separator + file.name)
                                if (!linkLocation.exists())
                                    Files.createSymbolicLink(
                                            linkLocation.absoluteFile.toPath(),
                                            Paths.get(this + File.separator + file.name)
                                    )
                            }
                }
            }

        }
        val client: CefClient = CefApp.getInstance(settings)
                .createClient()
                .also(GuiQueryHandler::initBrowserClient)

        /**
         * Retrieve the library path for JCEF. This is done to work around an issue, where the locales and resources path of jcef is no
         * correctly set up. The path is used to correctly set those paths in the [CefSettings] given to [CefApp.getInstance]
         */
        private fun getJcefLibPath(): String {
            val libraryPath = System.getProperty("java.library.path")
            val libraryPathSegments = libraryPath.split(System.getProperty("path.separator").toRegex()).dropLastWhile { it.isEmpty() }
            println(libraryPathSegments)
            for (libraryPath in libraryPathSegments) {
                val libraryPathFile = File(libraryPath)
                val expectedLibraries = libraryPathFile.list { _, file ->
                    file.equals("libjcef.dylib", ignoreCase = true) || file.equals("libjcef.so",
                            ignoreCase = true
                    ) || file.equals("jcef.dll", ignoreCase = true)
                }
                if (expectedLibraries != null && expectedLibraries.isNotEmpty()) {
                    return libraryPathFile.toPath().toAbsolutePath().normalize().toString()
                }
            }

            return libraryPath
        }
    }

    private var reloadScheduled = false

    /**
     * A [VibrantGuiDefinition] holding the meta data for this GUI
     */
    private val definition: VibrantGuiDefinition

    /**
     * The browser tab that is used to render and handle the GUI components
     */
    private var browserField: CefBrowserOsrLwjgl? = null

    val browser: CefBrowserOsrLwjgl
        get() = browserField!!

    /**
     * A GUI controller. This is a vanilla GuiScreen injected from the compatibility layer. It can be used to perform actions that normally
     * could be performed by the GuiScreen itself
     */
    lateinit var guiController: VibrantGuiController

    init {
        definition = gson
                .fromJson(this.javaClass.getResourceAsStream("/gui/$name/$name.json").reader().readText(), VibrantGuiDefinition::class.java)
        definition.init()

        initializeBrowser()
    }

    /**
     * Called ever client tick. If a reload is scheduled, it will call [initializeBrowser] to re-initialize the browser content
     */
    fun tickModule() {
        if (reloadScheduled) {
            initializeBrowser()
            reloadScheduled = false
        }
    }

    /**
     * @return an instance of a [VibrantGuiScreen] implementation that displays the contents defined by this [module][AbstractVibrantGuiManager]
     */
    open fun createGuiScreen(): VibrantGuiScreen {
        return GenericVibrantGuiScreen(guiModule = this)
    }

    /**
     * @param guiController an injected GuiScreen of vanilla Minecraft.
     *
     * @return an instance of a [VibrantGuiScreen] implementation that displays the contents defined by this [module][AbstractVibrantGuiManager]
     *
     * @see [VibrantGuiController]
     */
    open fun createGuiScreen(guiController: VibrantGuiController): VibrantGuiScreen {
        return GenericVibrantGuiScreen(guiController = guiController, guiModule = this)
    }

    /**
     * Schedule a reload of the browser tab rendering the GUI content. This must be scheduled, because the browser can only be accessed
     * within the browser thread. The actual reload is triggered by [tickModule]
     */
    fun scheduleReload() {
        reloadScheduled = true
    }

    /**
     * Initializes the [browser][CefBrowserOsrLwjgl] with its URL. It will free any resources used by previous instances. Can also be used
     * to reload the browser
     */
    private fun initializeBrowser() {
        if (browserField != null) {
            browserField!!.close()
        }

        browserField = CefBrowserOsrLwjgl(client, definition.interfaceUrl.toExternalForm(), true, null)
        browserField!!.resize(VibrantClient.minecraft.displayWidth, VibrantClient.minecraft.displayHeight)
    }
}


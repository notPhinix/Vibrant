package net.cydhra.vibrant.gui.mainmenu

import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.gui.QueryMethod
import net.cydhra.vibrant.gui.AbstractVibrantGuiManager
import org.cef.callback.CefQueryCallback

@Suppress("UNUSED_PARAMETER", "unused")
object MainMenuGuiManager : AbstractVibrantGuiManager("menu") {

    private const val ACTION_SELECT_OPTIONS_SCREEN = 0
    private const val ACTION_SELECT_WORLD_SCREEN = 1
    private const val ACTION_MULTIPLAYER_SCREEN = 2
    private const val ACTION_QUIT_GAME = 4
    private const val ACTION_LANGUAGE_SCREEN = 5

    @QueryMethod("singleplayer")
    fun displaySingleplayer(origin: CommandSender, label: String, callback: CefQueryCallback?) {
        VibrantClient.minecraft.scheduleTask(Runnable { guiController.actionPerformed(ACTION_SELECT_WORLD_SCREEN) })
    }

    @QueryMethod("multiplayer")
    fun displayMultiplayer(origin: CommandSender, label: String, callback: CefQueryCallback?) {
        VibrantClient.minecraft.scheduleTask(Runnable { guiController.actionPerformed(ACTION_MULTIPLAYER_SCREEN) })
    }

    @QueryMethod("options")
    fun displayOptions(origin: CommandSender, label: String, callback: CefQueryCallback?) {
        VibrantClient.minecraft.scheduleTask(Runnable { guiController.actionPerformed(ACTION_SELECT_OPTIONS_SCREEN) })
    }

    @QueryMethod("selectLanguage")
    fun displayLanguages(origin: CommandSender, label: String, callback: CefQueryCallback?) {
        VibrantClient.minecraft.scheduleTask(Runnable { guiController.actionPerformed(ACTION_LANGUAGE_SCREEN) })
    }

    @QueryMethod("quit")
    fun quitGame(origin: CommandSender, label: String, callback: CefQueryCallback?) {
        VibrantClient.minecraft.scheduleTask(Runnable { guiController.actionPerformed(ACTION_QUIT_GAME) })
    }
}

package net.cydhra.vibrant.command.commands

import com.google.gson.GsonBuilder
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

object IntegratedServerCommand : CommandHandler<IntegratedServerArguments>(userCommand = false) {

    private val gson = GsonBuilder().create()

    override fun executeCommand(origin: CommandSender, label: String, arguments: IntegratedServerArguments, callback: CefQueryCallback?) {
        when {
            arguments.listWorlds -> callback?.success(gson.toJson(VibrantClient.minecraft.getSaveLoaderInstance().getSaves()))
            arguments.launch -> {
                if (arguments.folder == null) {
                    callback?.failure(-1, "--folder cannot be null when trying to launch integrated server.")
                    return
                }

                val worldInfo = VibrantClient.minecraft.getSaveLoaderInstance().getSaves().find { it.fileName == arguments.folder }

                if (worldInfo == null) {
                    callback?.failure(-2, "world save ${arguments.folder} does not exist")
                    return
                }

                VibrantClient.minecraft.startIntegratedServer(arguments.folder!!, worldInfo.displayName, null)
                callback?.success("Launched integrated server with world ${arguments.folder}")
            }
        }
    }

}

class IntegratedServerArguments(parser: ArgParser) {
    val listWorlds by parser.flagging("-l", "--list", help = "List the available world files as a JSON array.")
    val launch by parser.flagging("-s", "--start", "--launch", help = "Launch the integrated server with a given world")

    val folder by parser.storing("-f", "--folder", help = "name of a folder where a world is saved").default<String?>(null)
}
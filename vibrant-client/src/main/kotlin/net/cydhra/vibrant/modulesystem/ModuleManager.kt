package net.cydhra.vibrant.modulesystem

import net.cydhra.eventsystem.EventManager
import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.VibrantClient.logger
import net.cydhra.vibrant.events.minecraft.KeyboardEvent
import net.cydhra.vibrant.modulesystem.ModuleManager.init
import net.cydhra.vibrant.modulesystem.ModuleManager.onKeyEvent

/**
 * A registry for [Module] implementations. They will get registered in [init] and are enabled when [onKeyEvent] handles a fitting [KeyboardEvent]
 */
object ModuleManager {

    private val registeredModuleLoaders = mutableListOf<ModuleLoader>()
    private val registeredModules = mutableListOf<Module>()

    val modules: List<Module> = registeredModules

    /**
     * Initialize manager. Registers [KeyboardEvent] handler and register all modules.
     */
    fun init() {
        logger.info("initializing module manager")
        EventManager.registerListeners(this)

        /*
         * A while loop is used, because modules are explicitly allowed to add more module loaders thus modifying the list.
         */
        var index = 0
        while (index < this.registeredModuleLoaders.size) {
            this.registerModules(this.registeredModuleLoaders[index++].loadModules().also { modules ->
                modules.forEach(Module::initialize)
            })
        }

        registeredModules.sortWith(kotlin.Comparator { m1: Module, m2: Module -> m2.displayName.length - m1.displayName.length })
    }

    private fun registerModules(modules: Collection<Module>) {
        logger.debug("registering modules ${modules.map { it.name }.joinToString(", ")}")
        this.registeredModules += modules
    }

    /**
     * Register a module
     * @param module module to be registered
     */
    private fun registerModule(module: Module) {
        logger.debug("registering module ${module.name}")
        this.registeredModules += module
    }

    fun registerModuleLoader(moduleLoader: ModuleLoader) {
        logger.debug("registering module loader ${moduleLoader.javaClass.simpleName}")
        this.registeredModuleLoaders += moduleLoader
    }

    @EventHandler
    fun onKeyEvent(e: KeyboardEvent) {
        if (e.type == KeyboardEvent.KeyboardEventType.PRESS) {
            this.modules.filter { it.keybind.keyCode == e.keycode }.forEach(Module::toggle)
        }
    }
}
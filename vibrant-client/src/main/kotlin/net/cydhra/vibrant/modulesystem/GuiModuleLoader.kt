package net.cydhra.vibrant.modulesystem

import com.google.common.reflect.ClassPath

private const val GUI_MODULE_PARENT_PACKAGE = "net.cydhra.vibrant.gui.modules"

/**
 * Load all modules regarding GUI components. This module loader is used by [net.cydhra.vibrant.modules.gui.GuiModule] to load further GUI
 * modules and register them
 */
class GuiModuleLoader : ModuleLoader {
    override fun loadModules(): List<Module> {
        return ClassPath
                .from(this.javaClass.classLoader)
                .getTopLevelClassesRecursive(GUI_MODULE_PARENT_PACKAGE)
                .map { it.load() }
                .filter { Module::class.java.isAssignableFrom(it) }
                .map { it.newInstance() as Module }
                .toList()
    }
}
package net.cydhra.vibrant.mixin.world;

import net.cydhra.vibrant.api.world.VibrantBlockInfo;
import net.cydhra.vibrant.api.world.VibrantChunk;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.world.chunk.Chunk;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Chunk.class)
public abstract class ChunkMixin implements VibrantChunk {

    @Shadow
    public abstract Block getBlock(final int x, final int y, final int z);

    @NotNull
    @Override
    public VibrantBlockInfo getBlockInChunk(int x, int y, int z) {
        return (VibrantBlockInfo) this.getBlock(x, y, z);
    }
}

package net.cydhra.vibrant.mixin.world.blocks;

import net.cydhra.vibrant.api.world.blocks.VibrantBlockStairs;
import net.minecraft.block.BlockStairs;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(BlockStairs.class)
public abstract class BlockStairsMixin implements VibrantBlockStairs {
}

package net.cydhra.vibrant.mixin.client;

import com.google.common.util.concurrent.ListenableFuture;
import net.cydhra.nidhogg.data.Session;
import net.cydhra.vibrant.api.client.VibrantGameSettings;
import net.cydhra.vibrant.api.client.VibrantMinecraft;
import net.cydhra.vibrant.api.client.VibrantPlayerController;
import net.cydhra.vibrant.api.client.VibrantTimer;
import net.cydhra.vibrant.api.entity.VibrantEntity;
import net.cydhra.vibrant.api.entity.VibrantPlayerSP;
import net.cydhra.vibrant.api.gui.VibrantGuiScreen;
import net.cydhra.vibrant.api.render.*;
import net.cydhra.vibrant.api.world.VibrantWorld;
import net.cydhra.vibrant.adapter.VibrantGlStateManagerImpl;
import net.cydhra.vibrant.adapter.VibrantGuiScreenAdapter;
import net.cydhra.vibrant.api.world.VibrantWorldSettings;
import net.cydhra.vibrant.api.world.storage.VibrantSaveFormat;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.entity.Entity;
import net.minecraft.util.Timer;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.storage.ISaveFormat;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.lang.reflect.Field;

@Mixin(Minecraft.class)
public abstract class MinecraftMixin implements VibrantMinecraft {

    @Shadow
    public EntityPlayerSP thePlayer;

    @Shadow
    public PlayerControllerMP playerController;

    @Shadow
    public WorldClient theWorld;

    @Shadow
    public RenderGlobal renderGlobal;

    @Shadow
    public EntityRenderer entityRenderer;

    @Shadow
    private Timer timer;

    @Shadow
    public GameSettings gameSettings;

    @Shadow
    public int displayWidth;

    @Shadow
    public int displayHeight;

    @Shadow
    public GuiScreen currentScreen;

    @Shadow
    private Entity renderViewEntity;

    @Shadow
    private Framebuffer framebufferMc;

    @Shadow
    private ISaveFormat saveLoader;

    @Shadow
    private net.minecraft.util.Session session;
    private Field sessionField;

    private final VibrantGlStateManager _vibrantGlStateManager = new VibrantGlStateManagerImpl();

    @Shadow
    public abstract void displayGuiScreen(GuiScreen screen);

    @Shadow
    public abstract TextureManager getTextureManager();

    @Shadow
    public abstract RenderManager getRenderManager();

    @Shadow
    public abstract ListenableFuture<Object> addScheduledTask(Runnable task);

    @Shadow
    public abstract void launchIntegratedServer(String folderName, String worldName, WorldSettings settings);

    @Nullable
    @Override
    public VibrantPlayerSP getThePlayer() {
        return (VibrantPlayerSP) this.thePlayer;
    }

    @Nullable
    @Override
    public VibrantPlayerController getPlayerController() {
        return (VibrantPlayerController) this.playerController;
    }

    @Nullable
    @Override
    public VibrantWorld getTheWorld() {
        return (VibrantWorld) this.theWorld;
    }

    @NotNull
    @Override
    public VibrantRenderGlobal getRenderGlobal() {
        return (VibrantRenderGlobal) this.renderGlobal;
    }

    @NotNull
    @Override
    public VibrantEntityRenderer getEntityRenderer() {
        return (VibrantEntityRenderer) this.entityRenderer;
    }

    @NotNull
    @Override
    public VibrantTileEntityRendererDispatcher getTileEntityRenderDispatcherInstance() {
        return (VibrantTileEntityRendererDispatcher) TileEntityRendererDispatcher.instance;
    }

    @NotNull
    @Override
    public VibrantTimer getTimer() {
        return (VibrantTimer) this.timer;
    }

    @NotNull
    @Override
    public VibrantGameSettings getGameSettings() {
        return (VibrantGameSettings) this.gameSettings;
    }

    @Override
    public void displayGuiScreen(@Nullable VibrantGuiScreen screen) {
        if (screen != null) { this.displayGuiScreen(new VibrantGuiScreenAdapter(screen)); } else {
            this.displayGuiScreen((GuiScreen) null);
        }
    }

    @Override
    public int getDisplayWidth() {
        return this.displayWidth;
    }

    @Override
    public int getDisplayHeight() {
        return this.displayHeight;
    }

    @Override
    public boolean isCurrentlyDisplayingScreen() {
        return this.currentScreen != null;
    }

    @NotNull
    @Override
    public VibrantEntity getTheRenderViewEntity() {
        return (VibrantEntity) this.renderViewEntity;
    }

    @Override
    public void setTheRenderViewEntity(@NotNull VibrantEntity vibrantEntity) {
        this.renderViewEntity = (Entity) vibrantEntity;
    }

    @NotNull
    @Override
    public Session getMinecraftSession() {
        return new net.cydhra.nidhogg.data.Session(
                this.session.getUsername(),
                this.session.getPlayerID(),
                this.session.getToken(),
                this.session.getSessionID()
        );
    }

    @Override
    public void setMinecraftSession(@NotNull Session session) {
        if (sessionField == null) {
            try {
                sessionField = Minecraft.class.getDeclaredField("session");
            } catch (NoSuchFieldException e) {
                throw new AssertionError();
            }
            sessionField.setAccessible(true);
        }

        try {
            sessionField.set(Minecraft.getMinecraft(),
                             new net.minecraft.util.Session(session.getAlias(), session.getId(), session.getAccessToken(), "mojang")
            );
        } catch (IllegalAccessException e) {
            throw new AssertionError();
        }
    }

    @NotNull
    @Override
    public VibrantGlStateManager getGlStateManager() {
        return this._vibrantGlStateManager;
    }

    @NotNull
    @Override
    public VibrantTessellator getTessellator() {
        return (VibrantTessellator) net.minecraft.client.renderer.Tessellator.getInstance();
    }

    @Nullable
    @Override
    public VibrantFramebuffer getCurrentFramebuffer() {
        return (VibrantFramebuffer) this.framebufferMc;
    }

    @NotNull
    @Override
    public VibrantTextureManager getTextureManagerInstance() {
        return (VibrantTextureManager) this.getTextureManager();
    }

    @NotNull
    @Override
    public VibrantRenderManager getRenderManagerInstance() {
        return (VibrantRenderManager) this.getRenderManager();
    }

    @NotNull
    @Override
    public ListenableFuture<Object> scheduleTask(@NotNull Runnable runnableToSchedule) {
        return this.addScheduledTask(runnableToSchedule);
    }

    @Override
    public void startIntegratedServer(@NotNull String folderName, @NotNull String worldName, @Nullable VibrantWorldSettings settings) {
        this.launchIntegratedServer(folderName, worldName, WorldSettings.class.cast(settings));
    }

    @NotNull
    @Override
    public VibrantSaveFormat getSaveLoaderInstance() {
        return (VibrantSaveFormat) this.saveLoader;
    }
}

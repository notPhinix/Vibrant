package net.cydhra.vibrant.mixin.entity;

import net.cydhra.vibrant.api.entity.VibrantPlayer;
import net.cydhra.vibrant.api.inventory.VibrantContainer;
import net.cydhra.vibrant.api.inventory.VibrantPlayerInventory;
import net.cydhra.vibrant.api.item.VibrantItemStack;
import net.cydhra.vibrant.api.util.VibrantDamageSource;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.entity.player.PlayerCapabilities;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(EntityPlayer.class)
public abstract class EntityPlayerMixin implements VibrantPlayer {

    @Shadow
    public InventoryPlayer inventory;

    @Shadow
    public Container openContainer;

    @Shadow
    public double chasingPosX;

    @Shadow
    public double chasingPosY;

    @Shadow
    public double chasingPosZ;

    @Shadow
    public PlayerCapabilities capabilities;

    @Shadow
    public abstract int getItemInUseCount();

    @Shadow
    public abstract ItemStack getHeldItem();

    @Override
    public double getChasingPosX() {
        return this.chasingPosX;
    }

    @Override
    public double getChasingPosY() {
        return this.chasingPosY;
    }

    @Override
    public double getChasingPosZ() {
        return this.chasingPosZ;
    }

    @NotNull
    @Override
    public VibrantPlayerInventory getPlayerInventory() {
        return (VibrantPlayerInventory) this.inventory;
    }

    @NotNull
    @Override
    public VibrantDamageSource getDamageSource() {
        return (VibrantDamageSource)
                net.minecraft.util.DamageSource.causePlayerDamage(EntityPlayer.class.cast(this));
    }

    @NotNull
    @Override
    public VibrantContainer getOpenContainer() {
        return (VibrantContainer) this.openContainer;
    }

    @Override
    public boolean isAllowedFlying() {
        return this.capabilities.allowFlying;
    }

    @Override
    public void setAllowedFlying(boolean allowedFlying) {
        this.capabilities.allowFlying = allowedFlying;
    }

    @Override
    public boolean isFlying() {
        return this.capabilities.isFlying;
    }

    @Override
    public void setFlying(boolean flying) {
        this.capabilities.isFlying = flying;
    }

    @Override
    public int getItemUsingCounter() {
        return this.getItemInUseCount();
    }

    @Nullable
    @Override
    public VibrantItemStack getItemHeld() {
        return VibrantItemStack.class.cast(this.getHeldItem());
    }
}

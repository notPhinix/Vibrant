package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.entity.VibrantEntity;
import net.cydhra.vibrant.api.render.VibrantRender;
import net.cydhra.vibrant.api.render.VibrantRenderManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(RenderManager.class)
public abstract class RenderManagerMixin implements VibrantRenderManager {

    @Shadow
    private double renderPosX;

    @Shadow
    private double renderPosY;

    @Shadow
    private double renderPosZ;

    @Shadow
    private boolean renderOutlines;

    @Shadow
    public abstract boolean renderEntitySimple(Entity entity, float partialTicks);

    @Shadow
    public abstract  <T extends Entity> Render<T> getEntityRenderObject(Entity entity);

    @Override
    public boolean renderEntitySimple(@NotNull VibrantEntity entity, float partialTicks, @Nullable Object unused) {
        return this.renderEntitySimple((Entity) entity, partialTicks);
    }

    @Override
    public double getRenderPosX() {
        return this.renderPosX;
    }

    @Override
    public void setRenderPosX(double renderPosX) {
        this.renderPosX = renderPosX;
    }

    @Override
    public double getRenderPosY() {
        return this.renderPosY;
    }

    @Override
    public void setRenderPosY(double renderPosY) {
        this.renderPosY = renderPosY;
    }

    @Override
    public double getRenderPosZ() {
        return this.renderPosZ;
    }

    @Override
    public void setRenderPosZ(double renderPosZ) {
        this.renderPosZ = renderPosZ;
    }

    @Override
    public boolean getRenderOutlines() {
        return this.renderOutlines;
    }

    @Override
    public void setRenderOutlines(boolean renderOutlines) {
        this.renderOutlines = renderOutlines;
    }

    @NotNull
    @Override
    public <T extends VibrantEntity> VibrantRender<T> getEntityRenderObj(@NotNull T entity) {
        return (VibrantRender<T>) this.getEntityRenderObject((Entity) entity);
    }
}

package net.cydhra.vibrant.mixin.client;

import net.cydhra.vibrant.api.client.VibrantPlayerController;
import net.cydhra.vibrant.api.entity.VibrantEntity;
import net.cydhra.vibrant.api.network.VibrantNetHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(PlayerControllerMP.class)
public abstract class PlayerControllerMixin implements VibrantPlayerController {

    @Shadow
    private NetHandlerPlayClient netClientHandler;

    @Shadow
    public abstract void attackEntity(EntityPlayer p_attackEntity_1_, Entity p_attackEntity_2_);

    @NotNull
    @Override
    public VibrantNetHandler getNetHandler() {
        return (VibrantNetHandler) this.netClientHandler;
    }

    @Override
    public void attackEntity(@NotNull VibrantEntity entity) {
        this.attackEntity(Minecraft.getMinecraft().thePlayer, (Entity) entity);
    }
}

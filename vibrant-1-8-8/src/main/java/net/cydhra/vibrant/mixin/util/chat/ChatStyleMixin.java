package net.cydhra.vibrant.mixin.util.chat;

import net.cydhra.vibrant.api.util.chat.EnumChatFormatting;
import net.cydhra.vibrant.api.util.chat.VibrantChatStyle;
import net.minecraft.util.ChatStyle;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(ChatStyle.class)
public abstract class ChatStyleMixin implements VibrantChatStyle {

    @Shadow
    public abstract ChatStyle setColor(net.minecraft.util.EnumChatFormatting color);

    @Shadow
    public abstract net.minecraft.util.EnumChatFormatting getColor();

    @Shadow
    public abstract ChatStyle setParentStyle(ChatStyle chatStyle);

    @Override
    public VibrantChatStyle setStyleColor(EnumChatFormatting formatting) {
        return (VibrantChatStyle) this.setColor(net.minecraft.util.EnumChatFormatting.values()[formatting.ordinal()]);
    }

    @Override
    public EnumChatFormatting getStyleColor() {
        return EnumChatFormatting.values()[this.getColor().ordinal()];
    }

    @Override
    public VibrantChatStyle setParentChatStyle(VibrantChatStyle style) {
        return (VibrantChatStyle) this.setParentStyle((ChatStyle) style);
    }
}

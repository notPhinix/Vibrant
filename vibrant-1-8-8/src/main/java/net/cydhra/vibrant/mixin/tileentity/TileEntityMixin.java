package net.cydhra.vibrant.mixin.tileentity;

import net.cydhra.vibrant.api.tileentity.VibrantTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(TileEntity.class)
public abstract class TileEntityMixin implements VibrantTileEntity {

    @Shadow
    public BlockPos pos;

    @Override
    public double getPosX() {
        return this.pos.getX();
    }

    @Override
    public double getPosY() {
        return this.pos.getY();
    }

    @Override
    public double getPosZ() {
        return this.pos.getZ();
    }
}

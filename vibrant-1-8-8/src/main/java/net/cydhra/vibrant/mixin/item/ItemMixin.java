package net.cydhra.vibrant.mixin.item;

import net.cydhra.vibrant.api.item.VibrantItem;
import net.minecraft.item.Item;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(Item.class)
public abstract class ItemMixin implements VibrantItem {
}

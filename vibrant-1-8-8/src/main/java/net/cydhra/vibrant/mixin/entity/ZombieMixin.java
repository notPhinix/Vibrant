package net.cydhra.vibrant.mixin.entity;

import net.minecraft.entity.monster.EntityZombie;
import net.cydhra.vibrant.api.entity.VibrantZombie;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(EntityZombie.class)
public abstract class ZombieMixin implements VibrantZombie {
}

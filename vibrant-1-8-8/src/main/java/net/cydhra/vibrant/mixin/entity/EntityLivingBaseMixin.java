package net.cydhra.vibrant.mixin.entity;

import net.cydhra.vibrant.api.entity.VibrantEntityLiving;
import net.cydhra.vibrant.api.util.VibrantDamageSource;
import net.cydhra.vibrant.api.util.VibrantVec3;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Vec3;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(EntityLivingBase.class)
public abstract class EntityLivingBaseMixin implements VibrantEntityLiving {

    @Shadow
    public abstract Vec3 getLookVec();

    @Shadow
    public abstract boolean isOnLadder();

    @NotNull
    @Override
    public VibrantDamageSource getDamageSource() {
        return (VibrantDamageSource)
                DamageSource.causeMobDamage(EntityLivingBase.class.cast(this));
    }

    @NotNull
    @Override
    public VibrantVec3 getLookVector() {
        return (VibrantVec3) this.getLookVec();
    }

    @Override
    public boolean isClimbing() {
        return this.isOnLadder();
    }
}

package net.cydhra.vibrant.mixin.client;

import net.cydhra.vibrant.api.client.VibrantTimer;
import net.minecraft.util.Timer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Timer.class)
public abstract class TimerMixin implements VibrantTimer {

    @Shadow
    public float renderPartialTicks;

    @Override
    public float getRenderPartialTicks() {
        return this.renderPartialTicks;
    }
}

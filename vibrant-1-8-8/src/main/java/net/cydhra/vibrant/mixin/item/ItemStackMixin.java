package net.cydhra.vibrant.mixin.item;

import net.cydhra.vibrant.api.item.VibrantItem;
import net.cydhra.vibrant.api.item.VibrantItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(ItemStack.class)
public abstract class ItemStackMixin implements VibrantItemStack {

    @Shadow
    private Item item;

    @NotNull
    @Override
    public VibrantItem getItem() {
        return (VibrantItem) this.item;
    }
}
